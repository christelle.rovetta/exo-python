# Exo Python

Exercices pour pratiquer python (avec un peu de math !)

Voici les notions abordées:

. Multmodulo : La face cachée des tables de multiplication
	- Python : fonctions, list, tuple, plot
	- Math : trigonometrie

. Got: Les personnages de GOT
	- Python : fonctions, list, comprehesion, classe (a venir)

. PFC : Jeu du pierre-feuille-ciseaux
	- Python: fonctions, conditions, boucle while
	- Math : proba discrete simple



